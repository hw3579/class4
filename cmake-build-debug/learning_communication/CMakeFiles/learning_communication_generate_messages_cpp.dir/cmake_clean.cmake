file(REMOVE_RECURSE
  "../devel/include/learning_communication/DoDishesAction.h"
  "../devel/include/learning_communication/DoDishesActionFeedback.h"
  "../devel/include/learning_communication/DoDishesActionGoal.h"
  "../devel/include/learning_communication/DoDishesActionResult.h"
  "../devel/include/learning_communication/DoDishesFeedback.h"
  "../devel/include/learning_communication/DoDishesGoal.h"
  "../devel/include/learning_communication/DoDishesResult.h"
  "CMakeFiles/learning_communication_generate_messages_cpp"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/learning_communication_generate_messages_cpp.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
