# CMake generated Testfile for 
# Source directory: /home/hw3579/Desktop/catkin_ws/src
# Build directory: /home/hw3579/Desktop/catkin_ws/src/cmake-build-debug
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("smb_common/smb_control")
subdirs("learning_communication")
subdirs("server01")
subdirs("smb_highlevel_controller")
subdirs("smb_common/smb_gazebo")
subdirs("smb_common/smb_description")
