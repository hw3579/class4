#include<ros/ros.h>
#include<roscpp_tutorials/TwoInts.h>//自定义service messages类型的头文件,需要预先编译,这里的例子已经预先由roscpp_tutorials编译了
#include<cstdlib>

int main(int argc, char **argv) 
/*
argc 是 argument count的缩写，表示传入main函数的参数个数；

argv 是 argument vector的缩写，表示传入main函数的参数序列或指针，
并且第一个参数argv[0]一定是程序的名称，并且包含了程序所在的完整路径，
所以确切的说需要我们输入的main函数的参数个数应该是argc-1个；

*/
{
ros::init(argc,argv,"add_two_ints_client");
if(argc != 3){
    ROS_INFO("usage: add_two_ints_client X Y");//打印信息流
    return 1;
}
ros::NodeHandle nh;
/*
初始化 ROS 节点是函数 ros::init () ，
启动节点是实例化类 ros::NodeHandle 也就是句柄，
这两步是 ROS 程序必不可少的，先后顺序不能变，否则会报错

*/
 //下面这一行很重要，它创建了以个serviceClient与已经创建的service（add_two_ints）进行通信
ros::ServiceClient client = nh.serviceClient<roscpp_tutorials::TwoInts>("add_two_ints");
//下一行跟上一行很重要，这里我们实例化一个自动生成的服务类，
//并为它的request成员赋值。一个服务类包括2个成员变量：request和response，
//以及2个类定义：Request和Response。
roscpp_tutorials::TwoInts service;
service.request.a = atoi(argv[1]);
service.request.b = atoi(argv[2]);
if (client.call(service)){
    ROS_INFO("sum:%ld",(long int)service.response.sum);
}else{
    ROS_ERROR("failed to call service add_two_ints");
    return 1;
}
return 0;
}
