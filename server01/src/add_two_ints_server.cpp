#include<ros/ros.h>
#include<roscpp_tutorials/TwoInts.h>

bool add(roscpp_tutorials::TwoInts::Request &request,
         roscpp_tutorials::TwoInts::Response &response)
{
response.sum=request.a+request.b;
ROS_INFO("request: x=%ld,y=%ld",(long int)request.a, 
                                (long int)request.b);
ROS_INFO(" sending back response:[%ld]",(long int)response.sum);
return true;


}

int main(int argc,char **argv)
{
ros::init(argc,argv,"add_two_ints_server");
ros::NodeHandle nh;
// 创建一个名为add_two_ints的server，注册回调函数add.
ros::ServiceServer service = nh.advertiseService("add_two_ints", add);
ros::spin();
//这句话的意思是循环且监听反馈函数add（callback）。
//循环就是指程序运行到这里，就会一直在这里循环了。
//监听反馈函数的意思是，如果这个节点有callback函数，那写一句ros::spin()在这里，
//就可以在有对应消息到来的时候，运行callback函数里面的内容。

return 0;



}
