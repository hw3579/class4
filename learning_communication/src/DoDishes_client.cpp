#include <actionlib/client/simple_action_client.h>//这是一个library里面一个做好的包（simple_action_server）里面的头文件
#include "learning_communication/DoDishesAction.h"//这个头文件是重点，在上一部分生成的action消息的头文件

typedef actionlib::SimpleActionClient<learning_communication::DoDishesAction> Client;
//客户端的定义写法
// 当服务器完成后会调用该回调函数一次
void doneCb(const actionlib::SimpleClientGoalState& state,
        const learning_communication::DoDishesResultConstPtr& result)
{
    ROS_INFO("Yay! The dishes are now clean");
    ros::shutdown();
}

// 当服务器激活后会调用该回调函数一次
void activeCb()
{
    ROS_INFO("Goal just went active");
}

// 收到feedback后调用该回调函数
void feedbackCb(const learning_communication::DoDishesFeedbackConstPtr& feedback)
{
    ROS_INFO(" percent_complete : %f ", feedback->percent_complete);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "do_dishes_client");

    // 定义一个客户端 开头直接写了
    Client client("do_dishes", true);

    // 等待服务器端
    ROS_INFO("Waiting for action server to start.");
    client.waitForServer();
    //client.waitForServer (), 客户端等待服务器函数，
    //可以传递一个ros::Duration作为最大等待值，程序进入到这个函数开始挂起等待，
    //服务器就位或者达到等待最大时间退出，前者返回true,后者返回false.
    ROS_INFO("Action server started, sending goal.");

    // 创建一个action的goal
    learning_communication::DoDishesGoal goal;
    goal.dishwasher_id = 1;//定义盘子的目标，也就是洗一个盘子

    // 发送action的goal给服务器端，并且设置回调函数
    client.sendGoal(goal,  &doneCb, &activeCb, &feedbackCb);

    ros::spin();

    return 0;
}

